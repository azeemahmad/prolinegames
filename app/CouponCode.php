<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CouponCode extends Model
{
    protected $table = 'coupon_code';
    protected $fillable = [
        'rs_100', 'rs_250', 'rs_500'
    ];

}
