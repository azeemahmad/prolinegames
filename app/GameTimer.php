<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameTimer extends Model
{
    protected $table = 'game_timers';
    protected $fillable = [
        'user_id', 'moves', 'starrating','time',
    ];

}
