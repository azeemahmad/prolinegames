<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proline</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link rel="stylesheet" href="css/app.css">

    <style type="text/css">
        tr, td, th {
            border: 1px solid lightgrey !important;
        }
        .popup{
            margin-top: 0px;
        }

        .popup-tnc {
            height: auto;
            overflow: hidden;
        }

        .popup-tnc .content {
            max-height: 100% !important;
            overflow: auto;
        }

        .popup-tnc .tnclist li {
            font-size: 20px;;
        }

        a.tc-title {
            color: #fff;
            background: #f42f24;
            font-size: 20px;
            padding: 5px;
            font-weight: 700;
            text-decoration: none;
        }
        .popup-tnc.container {
            display: grid;
            padding: 0px 5px;
        }
    </style>
</head>
<body>

<div class="container">
    <header>
        <a href="{{url('/')}}"><img src="img/logo.png"></a>
    </header>
    <div class="row"><!--popup-->
        <div class="container popup-tnc"><!--popup-tnc-->
            <h2>Terms & Conditions</h2>
            <div class="content">
                <ul class="tnclist">
                    <li>1. Voucher is valid at our Exclusive Proline Stores and <a href="https://www.prolineindia.com">Prolineindia.com</a>
                    </li>
                    <li>2. Vouchers are based on your time taken to finish the game.</li>
                    <li>3. Vouchers codes are sent to you via SMS/Email.</li>
                    <li>4. This voucher cannot be clubbed with any other offer.</li>
                    <li>5. Only one voucher can be redeemed at a time.</li>
                    <li> 6. For any questions please call our customer care number for details.</li>
                </ul>
                <table style="width: 95%;" border="0" cellspacing="0" cellpadding="0">
                    <colgroup>
                        <col width="201"/>
                        <col width="229"/>
                        <col width="116"/>
                        <col width="98"/>
                    </colgroup>
                    <tbody>
                    <!-- <tr style="height: 20px;">
                        <td><strong>Time to complete</strong></td>
                        <td><strong>Offer</strong></td>
                        <td><strong>Validity</strong></td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Above 45 secs to 60 secs</td>
                        <td>Rs. 100 off on shopping for Proline Apparel above 999</td>
                        <td>22nd Sep 2019</td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Between 30 secs to 45 sec</td>
                        <td>Rs. 250 off on shopping above 1499</td>
                        <td>22nd Sep 2019</td>
                    </tr>
                    <tr style="height: 20px;">
                        <td>Under 30 Seconds&nbsp;</td>
                        <td>Rs. 500 off on shopping above 1999</td>
                        <td>22nd Sep 2019</td>
                    </tr> -->

                    <tr>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl65" width="201" height="20">Complete the game in:</td>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl65" width="229">Offer&nbsp;</td>
                        <!-- <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl65" width="116">Valid from&nbsp;</td> -->
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl65" width="98">Valid to&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66" height="20">Under 15 Seconds  </td>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">Rs. 500 off on shopping  for Proline Apparel above 1999</td>
                        <!-- <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">23rd Sep 2019</td> -->
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">31st Oct 2019  &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66" height="20">Between 15 secs to 30 sec</td>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">Rs. 250 off on shopping  for Proline Apparel above 1499 &nbsp;</td>
                        <!-- <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">23rd Sep 2019&nbsp;&nbsp;</td> -->
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">31st Oct 2019 &nbsp;&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66" height="20"> Above 30 secs to 60 secs  &nbsp;</td>
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">Rs. 100 off on shopping for Proline Apparel above 999  &nbsp;</td>
                        <!-- <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">23rd Sep 2019&nbsp;&nbsp;</td> -->
                        <td class="m_635898453371566730m_5919395872868192490gmail-m_-6727410435306802964gmail-xl66">31st Oct 2019 &nbsp;&nbsp;&nbsp;</td>
                    </tr>
                    </tbody>
                </table>


            </div>
            <div><strong>&nbsp;</strong></div>
            <h2>Proline Store Locations :</h2>
            <ul class="tnclist">
                <li>1. Bangalore : Mantri Mall Malleshwaram :&nbsp;<a href="https://goo.gl/maps/4L2nS3Xp3cX8ZSe3A" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://goo.gl/maps/4L2nS3Xp3cX8ZSe3A&amp;source=gmail&amp;ust=1568457183746000&amp;usg=AFQjCNGjcxum2nkT1BSfIZ_XfJ9rc_1iJg">https://goo.gl/maps/<wbr />4L2nS3Xp3cX8ZSe3A</a>
                </li>
                <li>2. Bangalore : Total Mall Sarjapur Road :&nbsp;<a href="https://goo.gl/maps/dQDGR5HPCHRFFE5f9" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://goo.gl/maps/dQDGR5HPCHRFFE5f9&amp;source=gmail&amp;ust=1568457183746000&amp;usg=AFQjCNE2FTjIF500BlRkKzLGoD-0hEeAQw">https://goo.gl/maps/<wbr />dQDGR5HPCHRFFE5f9</a></li>
                <li>3. Bangalore : Inorbit Mall, Whitefield :&nbsp;<a href="https://goo.gl/maps/Vi5HanBeudxnPEor7" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://goo.gl/maps/Vi5HanBeudxnPEor7&amp;source=gmail&amp;ust=1568457183746000&amp;usg=AFQjCNF-rR9LNS7_ZoKFNpCQuEBNVHX_RA">https://goo.gl/maps/<wbr />Vi5HanBeudxnPEor7</a></li>
                <li>4. Hyderabad : Sharath City Mall :&nbsp;<a href="https://g.page/sarathcitycapitalmall?share" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://g.page/sarathcitycapitalmall?share&amp;source=gmail&amp;ust=1568457183746000&amp;usg=AFQjCNHTeOKcw3-m0rOH2rqq7k8j7VJOGw">https://g.page/<wbr />sarathcitycapitalmall?share</a></li>
                <li>5. Hyderabad : Himayat Nagar :&nbsp;<a href="https://goo.gl/maps/9gwZCoXxMBzcSHCV7" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://goo.gl/maps/9gwZCoXxMBzcSHCV7&amp;source=gmail&amp;ust=1568457183746000&amp;usg=AFQjCNHLxRnoN2cROW1LE5T1KwW9SD3eFg">https://goo.gl/maps/<wbr />9gwZCoXxMBzcSHCV7</a></li>
                <li>6. Mumbai : Inorbit Mall, Vashi :&nbsp;<a href="https://goo.gl/maps/UVQ6X2yCPuvZpbEZ6" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://goo.gl/maps/UVQ6X2yCPuvZpbEZ6&amp;source=gmail&amp;ust=1568457183746000&amp;usg=AFQjCNFNpbLOgBJPCBxrSEGfJ8gR2klnBg">https://goo.gl/maps/<wbr />UVQ6X2yCPuvZpbEZ6</a></li>
            </ul>
        </div>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script src="js/app.js"></script>
</body>
</html>
